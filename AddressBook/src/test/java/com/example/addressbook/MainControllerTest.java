package com.example.addressbook;

import com.example.addressbook.controllers.MainController;
import com.example.addressbook.models_table_address.Company;
import com.example.addressbook.models_table_address.Department;
import com.example.addressbook.models_table_address.Office;
import com.example.addressbook.models_table_address.Profile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails(value = "user")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/created-user-before.sql", "/profile-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/created-user-after.sql", "/profile-list-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)

public class MainControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MainController controller;

    @Test
    public void mainPageTest() throws Exception {
        this.mockMvc.perform(get("/main"))
                .andDo(print())
                .andExpect(authenticated());
    }

    @Test
    public void profileListTest() throws Exception {
        this.mockMvc.perform(get("/main"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(xpath("/html/body/main/section[1]").exists())
                .andExpect(xpath("/html/body/main/section[2]").exists())
                .andExpect(xpath("/html/body/main/section[3]").exists())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(10));
    }


    @Test
    public void profileFilterListTest() throws Exception {

        /**
         * Тест фильтра по имени
         */
        this.mockMvc.perform(MockMvcRequestBuilders.post("/filter")
                .with(csrf())
                .param("selectFilter", "1")
                .param("filter", "Иван")
        )
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(2));

        /**
         *  Тест фильта по должности
         */
        this.mockMvc.perform(MockMvcRequestBuilders.post("/filter")
                .with(csrf())
                .param("selectFilter", "2")
                .param("filter", "Садовод")
        )
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(3));

        /**
         *  Тест фильта по отделу
         */
        this.mockMvc.perform(MockMvcRequestBuilders.post("/filter")
                .with(csrf())
                .param("selectFilter", "3")
                .param("filter", "С_1")
        )
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(3));

        /**
         *  Тест фильта по офису
         */
        this.mockMvc.perform(MockMvcRequestBuilders.post("/filter")
                .with(csrf())
                .param("selectFilter", "4")
                .param("filter", "O_1")
        )
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(2));

        /**
         *  Тест фильта по компании
         */
        this.mockMvc.perform(MockMvcRequestBuilders.post("/filter")
                .with(csrf())
                .param("selectFilter", "5")
                .param("filter", "C_1")
        )
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(8));
    }

    @Test
    public void profileSaveTest() throws Exception {
        Profile profile = new Profile(
                "Константин",
                "Управляющий",
                "www.testMail@mail.ru",
                "+79056114256", new Department("P_1", new Office("O_1", new Company("C_1")))
        );
        this.mockMvc.perform(post("/save")
                .with(csrf())
                .flashAttr("profile", profile)
        )
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@id='thread-1']").nodeCount(11));
    }
}
