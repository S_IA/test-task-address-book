delete from profile_table;
delete from department_table;
delete from office_table;
delete from company_table;


insert into company_table(id, name, description) values
(17, 'C_1', '---'),
(18, 'C_2', '***');


insert into office_table(id, office_name, purpose, company_id ) values
(15, 'O_1', 'Северный', 17),
(16, 'O_2', 'Южный', 17),
(17, 'O_3', 'Центральный', 18);

insert into department_table(id, department_name, purpose, office_id ) values
(11, 'С_1', 'Производят', 16),
(12, 'С_2', 'Производят', 15),
(13, 'P_1', 'Обработка', 16),
(14, 'P_2', 'Обработка', 17);

insert into profile_table(id, name, position, email, phone, department_id) values
(1, 'Иван','Садовод','www.ivan@mail.ru','+79080102054', 11),
(2, 'Сергей','Гончар','www.Sergey@mail.ru','+79080202078', 13 ),
(3, 'Василий','Каменщик','www.Basil@mail.ru','+79100107264', 12 ),
(4, 'Константин','Плотник','www.Konstantin@mail.ru','+79180852014', 13 ),
(5, 'Петр','Управляющий','www.Peter@mail.ru','+79084451245', 14 ),
(6, 'Иван','Садовод','www.PS2@mail.ru','+79080102011', 11 ),
(7, 'Игорь','Садовод','www.PS3@mail.ru','+79080102874',11 ),
(8, 'Николай','Каменщик','www.PS4@mail.ru','+79080109554', 12 ),
(9, 'Александр','Управляющий','www.PS5@mail.ru','+79080562054', 14 ),
(10, 'Андрей','Плотник','www.PS6@mail.ru','+79081112054', 13 );

