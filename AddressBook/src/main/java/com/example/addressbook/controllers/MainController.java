package com.example.addressbook.controllers;

import com.example.addressbook.models_table_address.Company;
import com.example.addressbook.models_table_address.Department;
import com.example.addressbook.models_table_address.Office;
import com.example.addressbook.models_table_address.Profile;
import com.example.addressbook.repos.CompanyRepo;
import com.example.addressbook.repos.DepartmentRepo;
import com.example.addressbook.repos.OfficeRepo;
import com.example.addressbook.repos.ProfileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private ProfileRepo profileRepo;
    @Autowired
    private DepartmentRepo departmentRepo;
    @Autowired
    private OfficeRepo officeRepo;
    @Autowired
    private CompanyRepo companyRepo;


    @GetMapping("/")
    public String greeting(Model model) {
        Iterable<Profile> profiles = profileRepo.findAll();
        model.addAttribute("profiles", profiles);
        return "main";
    }


    @GetMapping("/main")
    public String main(Model model) {
        Iterable<Profile> profiles = profileRepo.findAll();
        model.addAttribute("profiles", profiles);
        return "main";
    }

    @PostMapping("/tableRows")
    public String helloy(Model model) {

        Iterable<Profile> profiles = profileRepo.findAll();
        return "main";
    }


    @PostMapping("/save")
    public String save(
            @ModelAttribute Profile profile,
            Model model
    ) {

        Company company = companyRepo.findByNameLike(profile.getDepartment().getOffice().getCompany().getName());

        if (company != null) {

            Office office = officeRepo.findOfficeByNameAndCompany(company.getId(), profile.getDepartment().getOffice().getName());
            if (office != null) {

                Department department = departmentRepo.findDepartmentByNameAndOffice(office.getId(), profile.getDepartment().getName());
                if (department != null) {
                    profile.setDepartment(department);
                }
                profile.getDepartment().setOffice(office);
            }
            profile.getDepartment().getOffice().setCompany(company);
        }


        profileRepo.save(profile);
        model.addAttribute("profiles", profileRepo.findAll());
        return "main";
    }


    @PostMapping("/filter")
    public String filter(
            @RequestParam(value = "selectFilter") int selectFilter,
            @RequestParam(value = "filter") String filter,
            Model model) {

        List<Profile> profiles = new ArrayList<Profile>();

        if (filter != null && !filter.isEmpty()) {
            //выборка по имени
            if (selectFilter == 1) {
                profiles = profileRepo.findByName(filter);
            }
            //выборка по должности
            if (selectFilter == 2) {
                    profiles = profileRepo.findByPosition(filter);
            }
            //выборка по департаменту
            if (selectFilter == 3) {
                profiles = profileRepo.findByProfileDepartment(filter);
            }
            //выборка по офису
            if (selectFilter == 4) {
                profiles = profileRepo.findByProfileOffice(filter);
            }
            //выборка по компании
            if (selectFilter == 5) {
                profiles = profileRepo.findByProfileCompany(filter);
            }
        } else {
            profiles = profileRepo.findAll();
        }


        model.addAttribute("profiles", profiles);
        return "main";
    }


}
