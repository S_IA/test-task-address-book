package com.example.addressbook.controllers;

import com.example.addressbook.models_table_address.Profile;
import com.example.addressbook.repos.ProfileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ProfileController {

    @Autowired
    private ProfileRepo profileRepo;

    @CrossOrigin
    @GetMapping("/getProfile/{id}")
    private Map<String, String> getProfile(Model model,
                                           @PathVariable("id") Integer id){
        Profile profile =profileRepo.findById(id).get();
        HashMap<String, String> map = new HashMap<>();
        map.put("name", profile.getName());
        map.put("position", profile.getPosition());
        map.put("phone", profile.getPhone());
        map.put("mail", profile.getEmail());
        return map;
    }
}
