package com.example.addressbook.models_table_address;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="Department_table")
public class Department implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "department_name")
    private String name;
    @Column(name = "purpose")
    private String purposeDepartment;


    @OneToMany(mappedBy = "department")
    private Set<Profile> profiles;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "office_id")
    private Office office;

    public Department() {
    }

    public Department(String name) {
        this.name = name;
    }

    public Department(Integer id, String name, String purpose) {
        this.id = id;
        this.name = name;
        this.purposeDepartment = purpose;
    }


    public Department(String name, Office office) {
        this.name = name;
        this.office = office;
    }

    public String getOfficeName(){
        return office != null ? office.getName() :"<none>";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurpose() {
        return purposeDepartment;
    }

    public void setPurpose(String purpose) {
        this.purposeDepartment = purpose;
    }

    public String getPurposeDepartment() {
        return purposeDepartment;
    }

    public void setPurposeDepartment(String purposeDepartment) {
        this.purposeDepartment = purposeDepartment;
    }

    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }
}
