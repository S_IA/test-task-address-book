package com.example.addressbook.models_table_address;



import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Office_table")
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "office_name")
    private String name;
    @Column(name = "purpose")
    private String purpose;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id")
    private Company company;


    @OneToMany(mappedBy = "office")
    private Set<Department> departments;

    public Office(Integer id, String name, String purpose, Company company, Set<Department> departments) {
        this.id = id;
        this.name = name;
        this.purpose = purpose;
        this.company = company;

    }


    public Office(){}

    public Office( String name) {

        this.name = name;

    }

    public Office(Integer id, String name, String purpose, Company company) {
        this.id = id;
        this.name = name;
        this.purpose = purpose;

    }

    public Office(String name, Company company){
        this.name = name;
        this.company = company;
    }

    public String getCompanyName(){
        return company != null ? company.getName() :"<none>";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }


}
