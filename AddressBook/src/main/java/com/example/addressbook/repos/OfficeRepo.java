package com.example.addressbook.repos;

import com.example.addressbook.models_table_address.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface OfficeRepo extends JpaRepository<Office, Long> {
    Office findByNameLike(String name);

    @Query("SELECT o FROM Office o WHERE COMPANY_ID= ?1  and OFFICE_NAME= ?2 ")
    Office findOfficeByNameAndCompany(Integer id, String name);
}
