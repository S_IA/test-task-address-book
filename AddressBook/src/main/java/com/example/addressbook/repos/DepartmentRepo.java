package com.example.addressbook.repos;

import com.example.addressbook.models_table_address.Department;
import com.example.addressbook.models_table_address.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DepartmentRepo extends JpaRepository<Department, Long> {

    Department findByNameLike(String name);

    @Query("SELECT d FROM Department d WHERE OFFICE_ID= ?1 and DEPARTMENT_NAME= ?2")
    Department findDepartmentByNameAndOffice(Integer office_id, String name);

    List<Department> findByName(String filter);
}
