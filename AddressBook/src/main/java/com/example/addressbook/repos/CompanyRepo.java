package com.example.addressbook.repos;

import com.example.addressbook.models_table_address.Company;
import com.example.addressbook.models_table_address.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CompanyRepo extends JpaRepository<Company, Long> {
    Company findByNameLike(String name);

}
