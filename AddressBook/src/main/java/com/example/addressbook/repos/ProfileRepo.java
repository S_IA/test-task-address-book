package com.example.addressbook.repos;

import com.example.addressbook.models_table_address.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProfileRepo extends JpaRepository<Profile, Integer> {

    List<Profile> findByName(String name);

    List<Profile> findByPosition(String filter);

    @Query("SELECT p FROM Profile p  join Department d " +
            "on d.id = p.department   where d.name= ?1  ")
    List<Profile> findByProfileDepartment(String filter);

    @Query("SELECT p FROM Profile p  " +
            "join Department d on d.id = p.department " +
            "join Office o on o.id = d.office  where o.name= ?1  ")
    List<Profile> findByProfileOffice(String filter);

    @Query("SELECT p FROM Profile p  " +
            "join Department d on d.id = p.department " +
            "join Office o on o.id = d.office " +
            "join Company c on c.id = o.company where c.name= ?1  ")
    List<Profile> findByProfileCompany(String filter);
}
